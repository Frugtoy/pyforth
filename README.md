# PyForth



## Обычный интерпретатор Forth, ничего серьёзного.
Для реализации создадим класс PyForth

Отделим символы от логики, чтобы давать возможность  
### Пример проброса (функции coming soon):
```
forth = ForthInterpreter()

forth.define('+', forth.add)

forth.define('-', forth.subtract)

forth.define('*', forth.multiply)

forth.define('/', forth.divide)

forth.define('%', forth.modulus)

forth.define('dup', forth.dup)

forth.define('drop', forth.drop)

forth.define('swap', forth.swap)

forth.define('if', forth._if)

forth.define('loop', forth._loop)
```




P.S: Программа должна в качестве теста принимать на вход .forth файл

P.P.S: Для "-Хоть какого то интереса" требуется добавить в .forth файл логику поиска корней кубического уравнения вида:
```ax^3 + bx^2 +cx + d = 0```